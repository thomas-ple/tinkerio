import numpy as np
RNG=np.random.default_rng()

def bootstrap_analysis(data:np.ndarray,n_iter=1000,return_samples=False):
  bootstrap_samples:np.ndarray=RNG.choice(
    data,size=(n_iter,data.shape[0]),replace=True).mean(axis=1)
  
  if return_samples: return bootstrap_samples
  return bootstrap_samples.mean(axis=0),bootstrap_samples.std(axis=0)

def blocking_std(data,mode="weighted",ax=None):
  n=len(data)
  if n<2: return np.NaN
  blockdat=np.copy(data)
  mean=data.mean()
  std=np.std(blockdat)/np.sqrt(n-1)
  stds=[]
  stderrs=[]
  stds.append(std)
  stderrs.append(std/np.sqrt(2*(n-1)))
  while True:
    blockdat = 0.5*(blockdat[:-1:2]+blockdat[1::2])
    n=len(blockdat)
    if n<2: break
    std=np.std(blockdat)/np.sqrt(n-1)
    stds.append(std)
    stderrs.append(std/np.sqrt(2*(n-1)))
  stds=np.array(stds)
  stderrs=np.array(stderrs)
  imax=np.argmax(stds)
  stdmax=stds[imax]
  imaxmin=np.argmax(stds-stderrs)
  stdmaxmin=stds[imaxmin]
  if mode=="naive":
    std = stds[0]
  elif mode=="max":
    std = stdmax
  elif mode=="maxmin":
    std = stdmaxmin
  elif mode=="weighted":
    wmax=1./(stderrs[imax]+1.e-8)
    wmaxmin=1/(stderrs[imaxmin]+1.e-8)
    std=(stdmax*wmax + stdmaxmin*wmaxmin)/(wmax+wmaxmin)
  else:
    raise Exception(f"Blocking mode {mode} not recognized")
  #std=0.5*(np.max(stds)+stds[np.argmax(stds-stderrs)])
  if ax is not None:
    ax.plot(stds)
    ax.fill_between(np.arange(len(stds)),stds-stderrs,stds+stderrs,alpha=0.5)
    ax.axvline(np.argmax(stds),color="red")
    ax.axvline(np.argmax(stds-stderrs),color="green")
    ax.axhline(std,color="k")
  #print(np.max(stds),stds[np.argmax(stds-stderrs)])
  return std

def cumavg(data):
  return np.cumsum(data,axis=0)/(np.arange(data.shape[0])+1.)[:,None]