#!/usr/bin/env python3
import sys
import time
import argparse
from pathlib import Path
import numpy as np
import mdtraj as md
from numba import njit

from .xyztraj import xyz_reader,get_xyz_info
from .util import TinkerScript

def apply_pbc(vec,cell):
  cellinv = np.linalg.inv(cell)
  q = cellinv @ vec.T
  q-=np.rint(q)
  return (cell @ q).T

@njit
def apply_pbc_numba(vec,cell,cellinv):
  q = cellinv @ vec.T
  q-=np.rint(q)
  return (cell @ q).T

@njit
def pbc_distance(x1,x2,box=None):
  dx=x1-x2
  if box is not None: 
    dx[:] -= box[:3]*np.rint(dx[:]/box[:3])
  return np.linalg.norm(dx)

def get_pairs(species,sp1=None,sp2=None):
  #nat=len(species)
  #id1=np.flatnonzero(species==sp1) if sp1 is not None else np.arange(nat)
  #if sp2 != sp1:
  #  id2=np.flatnonzero(species==sp2) if sp2 is not None else np.arange(nat)
  #pairs=np.array([np.repeat(id1, len(id2)),np.tile(id2, len(id1))])
  #if sp1 is None or sp2 is None or sp1==sp2:
  #  pairs=pairs[:,pairs[0]!=pairs[1]]
  #return pairs
  #if sp1 is None and sp2 is not None: sp1=sp2
  #if sp2 is None and sp1 is not None: sp2=sp1
  if sp1 is None:
    if sp2 is None: 
      return np.array(np.triu_indices(len(species),1)),0.5*len(species)**2
    sp1=sp2
  id1=np.flatnonzero(species==sp1)
  num1=len(id1)
  if sp2==sp1 or sp2 is None:
    pairsloc=np.triu_indices(len(id1),1)
    return np.array([id1[pairsloc[0]],id1[pairsloc[1]]]),0.5*num1**2
  id2=np.flatnonzero(species==sp2)
  num2=len(id2)
  return np.array([np.repeat(id1, len(id2)),np.tile(id2, len(id1))]),num1*num2
    
@njit()
def radial_density_numba(species,xyz,box=None,rmax=10.0,dr=0.1,sp1=None,sp2=None):
  n_atoms=len(species)
  n_bins=int(rmax/dr)
  r=np.zeros(n_bins)
  n_pairs=0
  check_sp1=sp1 is not None
  check_sp2=sp2 is not None
  for i in range(n_atoms):
    if check_sp1 and species[i] != sp1: continue
    for j in range(n_atoms):
      if check_sp2 and species[j] != sp2: continue
      if i == j: continue
      n_pairs+=1
      rij=pbc_distance(xyz[i,:],xyz[j,:],box)
      if rij > rmax:
        continue
      rij_bin=int(rij/dr)
      r[rij_bin]+=1
  numj=0
  numk=0
  for i in range(n_atoms):
    if check_sp1 and species[i] == sp1: numj+=1
    if check_sp2 and species[i] == sp2: numk+=1
  n_pairs = numj*numk

  ii=np.arange(n_bins)
  expect = 4./3.*np.pi*dr**3*((ii+1)**3 - ii**3)
  if box is not None:
    volume=box[0]*box[1]*box[2]
    expect*=n_pairs/volume
  return r/expect

@njit()
def radial_densities(species,xyz,cell=None,rmax=10.0,dr=0.1):
  n_atoms=len(species)
  n_bins=int(rmax/dr)
  at_set=set(species)
  nspecies=len(at_set)
  sp_pairs={}
  c=0
  cellinv=np.linalg.inv(cell) if cell is not None else None
  for i in range(nspecies-1):
    for j in range(i+1,nspecies):
      sp_pairs[at_set[i]+at_set[j]]=c
      c+=1
  r=np.zeros((n_bins,c))
  n_pairs=0
  for i in range(n_atoms):
    for j in range(n_atoms):
      if i == j: continue
      scat=species[i]+species[j]
      if(scat not in sp_pairs): continue
      icol=sp_pairs[scat]
      n_pairs+=1
      vec = xyz[i,:]-xyz[j,:]
      if cell is not None: vec=apply_pbc_numba(vec,cell,cellinv)
      rij=np.linalg.norm(vec)
      if rij > rmax:
        continue
      rij_bin=int(rij/dr)
      r[rij_bin,icol]+=1
  print(n_pairs)
  bins=np.linspace(0,rmax,n_bins+1)
  expect = n_pairs*4./3.*np.pi*((bins[1:])**3 - bins[:-1]**3)
  if cell is not None:
    volume=np.dot(cell[:,0],np.cross(cell[:,1],cell[:,2]))
    expect/=volume
  return r/expect

@njit
def radial_histogram(xyz,id1,id2,rmax,dr,box):
  n_bins=int(rmax/dr)
  r=np.zeros(n_bins)
  n_pairs=0
  for i in id1:
    for j in id2:
      if i == j: continue
      n_pairs+=1
      rij=pbc_distance(xyz[i,:],xyz[j,:],box)
      if rij > rmax:
        continue
      rij_bin=int(rij/dr)
      r[rij_bin]+=1
  return r,n_pairs

def radial_density_smart(species,xyz,box=None,rmax=10.0,dr=0.1,sp1=None,sp2=None):
  n_atoms=len(species)
  id1=np.flatnonzero(species==sp1) if sp1 is not None else np.arange(n_atoms)
  id2=np.flatnonzero(species==sp2) if sp2 is not None else np.arange(n_atoms)
  r,n_pairs=radial_histogram(xyz,id1,id2,rmax,dr,box)
  #print(n_pairs)
  n_bins=len(r)
  bins=np.linspace(0,rmax,n_bins+1)
  expect = 4./3.*np.pi*((bins[1:])**3 - bins[:-1]**3)
  if box is not None:
    volume=box[0]*box[1]*box[2]
    expect*=n_pairs/volume
  return r/expect

def radial_density(species,xyz,cell=None,rmax=10.0,dr=0.1,sp1=None,sp2=None,pairs=None,timer=False):
  nat = len(species)
  if timer: 
    print(sp1,sp2)
    time0=time.time()
  if pairs is None:
    pairs,n_pairs_norm=get_pairs(species,sp1,sp2)
  n_pairs=pairs.shape[1]
  mult = 1 if sp1!=sp2 else 2
  #print(mult*n_pairs,mult)
  if timer:
    print("pairs:",time.time()-time0)
    time0=time.time()
  vec=xyz[pairs[0]]-xyz[pairs[1]]
  if cell is not None:
    vec=apply_pbc(vec,cell)
  dist=np.linalg.norm(vec,axis=1)
  if timer: 
    print("dists:",time.time()-time0)
    time0=time.time()
  n_bins=int(rmax/dr)
  bins=np.linspace(0,rmax,n_bins+1)
  r,_=np.histogram(dist,bins=n_bins,range=(0,rmax))
  if timer: print("hist:",time.time()-time0)
  expect = n_pairs_norm*4./3.*np.pi*((bins[1:])**3 - bins[:-1]**3)
  if cell is not None:
    volume=np.dot(cell[:,0],np.cross(cell[:,1],cell[:,2]))
    expect/=volume
  return r/expect


def radial_from_file(xyzfile,pairs,outfile="gr.dat",rmax=10,dr=0.02,thermalize=0,stride=1,box_info=False,indexed=False,watch=False,cell=None,dcd=False,**kwargs):
  if kwargs["tinker"] or kwargs["dcd"]:
    indexed=True
    box_info=True
  
  try:
    #frames = list(read_arc_file(arcfile,max_frames=205))
    #print(len(frames))
    if dcd:
      species = get_xyz_info(xyzfile,box_info,indexed)
      dcdfile = xyzfile[:-4]+".dcd"
      reader = md.iterload(dcdfile,top=xyzfile,chunk=1
                ,stride=stride,skip=thermalize)
    else:
      reader=xyz_reader(xyzfile,box_info=box_info,indexed=indexed
                ,start=thermalize+1,step=stride,stream=watch)
    n_bins=int(rmax/dr)
    centers=(np.arange(n_bins)+0.5)*dr 
    nframe=0
    if cell is not None:
      cell=np.array([float(c) for c in cell.split()]).reshape((3,3),order="F")
      print("cell matrix=",cell)
    #setup_figures(["g(r)"])
    #plt.pause(1.)
    pairs = [p.strip().replace("-"," ").replace("_"," ") for p in pairs]
    header = " ".join(["r"]+["-".join(p.split()) for p in pairs])
    cell=None
    box=None
    gr_avg=np.zeros((n_bins,len(pairs)))
    for frame in reader:
      if dcd:
        xyz=frame.xyz[0]*10.
        box=np.zeros(6)
        box[:3]=frame.unitcell_lengths[0]*10.
        box[3:]=frame.unitcell_angles[0]
        if box_info: cell = frame.unitcell_vectors[0]*10.
      else:
        species,xyz,box=frame
        if box is not None:
          cell=np.diag(box[:3])
      nframe+=1
      print(nframe)
      for i,pair in enumerate(pairs):
        sp1,sp2=pair.split()
        gr=radial_density(species,xyz,cell,rmax,dr,sp1=sp1,sp2=sp2,timer=False)
        #gr=radial_density_numba(species,xyz,box,rmax,dr,sp1=sp1,sp2=sp2)
        gr_avg[:,i]+=gr
      #cleanup_ax(plt.gca())
      #plt.plot(centers,gr_avg/nframe)
      #plt.pause(1.)
      np.savetxt(outfile.strip(),np.column_stack((centers,gr_avg/nframe)),header=header)
      
  except KeyboardInterrupt:
    sys.exit(0)


def populate_parser(parser:argparse.ArgumentParser):
  parser.add_argument("xyzfile",help="input xyz file",type=Path)
  parser.add_argument("pairs",help="pairs of types (for example 'O O' 'O H' 'H H')",type=str,nargs="+")
  parser.add_argument("--rmax",help="max radius",type=float,default=10.)
  parser.add_argument("--dr",help="bin width",type=float,default=0.02) 
  parser.add_argument("-t","--thermalize",help="number of frames to skip for thermalization",type=int,default=0)
  parser.add_argument("--stride",help="stride between frames",type=int,default=1)
  parser.add_argument("-i","--indexed",help="are the atoms indexed ? (Tinker arc)",action="store_true")
  parser.add_argument("-b","--box_info",help="second line of each frame contains box information",action="store_true")
  parser.add_argument("-w","--watch",help="stream the data",action="store_true")
  parser.add_argument("-T","--tinker",help="flag for tinker arc file",action="store_true")
  parser.add_argument("--dcd",help="flag dcd file",action="store_true")
  parser.add_argument("-o","--outfile",help="output file name",type=Path,default="gr.dat")
  parser.add_argument("--cell",help="cell vectors formatted as 'xa ya za xb yb zb xc yc zc'",type=str)
  return parser

def main():
  parser=argparse.ArgumentParser(description="exctract average or watch tinker properties")
  kwargs=vars(parser.parse_args())
  radial_from_file(**kwargs)

radial_script=TinkerScript('radial',radial_from_file,populate_parser,aliases=["r"]
                ,description="compute radial pair distribution functions from .xyz, .arc or .dcd trajectory"
                ,help="compute radial distibution functions")

if __name__ == "__main__":
  main()