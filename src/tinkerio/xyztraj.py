import numpy as np
import time
from numba import njit

def clean_split_line(line):
  lineclean=line.strip()
  #skip comments
  if lineclean.startswith("#"):
    return None
  #split line
  parsed_line=lineclean.split()
  #remove empty strings
  parsed_line=[x for x in parsed_line if x]
  if parsed_line:
    return parsed_line
  else:
    return None

def get_xyz_info(filename,box_info=False,indexed=False):
  atoms=[]
  shift=1 if indexed else 0
  with open(filename,"r") as f:
    n_atoms=int(f.readline().split()[0])
    if box_info: f.readline()
    for i in range(n_atoms):
      line=f.readline()
      parsed_line=clean_split_line(line)
      if parsed_line is None:
        continue
      atoms.append(parsed_line[shift])

  if len(atoms) != n_atoms:
    print("Error: number of atoms does not match data!")
    print(len(atoms),n_atoms)
    raise ValueError
	
  return np.array(atoms,dtype='<U2')


def read_xyz_step(pf,n_atoms,positions):
  read_nat=False
  nat_read=0
  if positions.shape != (n_atoms,3):
    print("Error: incorrect position array shape!")
    print(positions.shape,(n_atoms,3))
    raise ValueError
  while True:
    line=pf.readline()
    if not line:
      return True
    parsed_line=clean_split_line(line)
    if parsed_line is None:
      continue
    if not read_nat:
      #read number of atoms
      nat_new=int(parsed_line[0])
      if nat_new != n_atoms:
        print("Error: number of atoms cannot change!")
        print(nat_new,n_atoms)
        raise ValueError
      read_nat=True
      continue
		
    for i in range(3):
      positions[nat_read,i]=float(parsed_line[i+1])
    nat_read+=1
    if(nat_read>=n_atoms):
      break
  return False	

def get_n_xyz_steps(pf,N,n_atoms):
  positions=np.zeros((n_atoms,3,N))
  eof=False
  for i in range(N):
    eof=read_xyz_step(pf,n_atoms,positions[:,:,i])
    if eof:
      return None
  return positions

def xyz_reader(filename,box_info=False,indexed=False
                  ,start=1, stop=-1,step=1,max_frames=None
                  ,stream=False,interval=2.,sleep=time.sleep):
  if stop>0 and start > stop: return
  pf=open(filename,"r")
  inside_frame=False
  nat_read=False
  iframe=0
  stride=step
  nframes=0
  if start>1 : print(f"skipping {start-1} frames")
  if not box_info: box=None
  while True:
    line=pf.readline()

    if not line:
      if stream:
        sleep(interval)
        continue
      elif inside_frame or nat_read:
        raise Exception("Error: premature end of file!")
      else:
        break
    
    line = line.strip()
    if line.startswith("#"): continue

    if not inside_frame:
      if not nat_read:
        nat=int(line.split()[0])
        nat_read=True
        iat=0
        inside_frame = not box_info
        xyz=np.zeros((nat,3))
        species=[]
        continue
      
      box=np.array(line.split(),dtype="float")
      inside_frame=True
      continue

    ls=line.split()
    iat,s = (int(ls[0]),1) if indexed else (iat+1,0)
    species.append(ls[s])
    xyz[iat-1,:] = np.array([ls[s+1],ls[s+2],ls[s+3]],dtype="float")
    if iat == nat:
      iframe+=1
      inside_frame=False
      nat_read=False
      if iframe < start: continue
      stride+=1
      if stride>=step:
        nframes+=1
        stride=0
        yield np.array(species,dtype='<U2'),xyz,box
      if (max_frames is not None and nframes >= max_frames) \
          or (stop>0 and iframe >= stop):
        break