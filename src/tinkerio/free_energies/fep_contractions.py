#!/usr/bin/env python3
import sys
import numpy as np
import sys
import os
import argparse
from pathlib import Path

from ..util import TinkerScript,compose
from . import BarTraj,populate_parser_bar,fe_perturbation,bar
from ..units import GASCONST

def fep_PI_contraction(filename,filename_ctr,useb=False,**kwargs):
  verbose_save=False
  if "verbose" in kwargs: verbose_save=kwargs["verbose"]
  kwargs["verbose"]=False
  if useb:
    _,(_,u,vol,temp,_)    = BarTraj.read_bar_file(filename,**kwargs) 
    _,(_,uctr,vol,temp,_) = BarTraj.read_bar_file(filename_ctr,**kwargs) 
  else:
    (u,_,vol,temp)   ,_ = BarTraj.read_bar_file(filename,**kwargs) 
    (uctr,_,vol,temp),_ = BarTraj.read_bar_file(filename_ctr,**kwargs) 

  kwargs["verbose"]=verbose_save
  assert u.shape[0]==uctr.shape[0]
  traj = BarTraj(uctr,u,vol,temp)
  kwargs["shiftavg"]=True
  return fe_perturbation(traj,**kwargs)

def PI_contraction_weights(filename,filename_ctr,**kwargs):
  traj_a,traj_b = BarTraj.read_bar_file(filename,**kwargs) 
  trajctr_a,trajctr_b = BarTraj.read_bar_file(filename_ctr,**kwargs)

  rta = traj_a.temp*GASCONST
  rtb = traj_b.temp*GASCONST

  dua=trajctr_a.u0-traj_a.u0
  dub=trajctr_b.u1-traj_b.u1

  return traj_a.with_bias(dua),traj_b.with_bias(dub)

def bar_PI_contraction(filename,filename_ctr,**kwargs):
  traj_a,traj_b = PI_contraction_weights(filename,filename_ctr,**kwargs)
  return bar(traj_a,traj_b,**kwargs)

def bar_PI_contraction_indirect(filename,filename_ctr,useb=False,**kwargs):
  traj_a,traj_b = BarTraj.read_bar_file(filename,**kwargs) 
  trajctr_a,trajctr_b = BarTraj.read_bar_file(filename_ctr,**kwargs)

  if useb: 
    dub=trajctr_b.u1-traj_b.u1
    return bar(trajctr_a,traj_b.with_bias(dub),**kwargs)
    
  dua=trajctr_a.u0-traj_a.u0
  return bar(traj_a.with_bias(dua),trajctr_b,**kwargs)
  
  

def populate_parser_contract(parser:argparse.ArgumentParser):
  parser.add_argument("filename",help="Tinker bar file for full polymer",type=Path)
  parser.add_argument("filename_ctr",help="Tinker bar file for contracted polymer",type=Path)
  return parser

def populate_parser_useb(parser:argparse.ArgumentParser):
  parser.add_argument("-b","--useb",action='store_true',help="use B trajectory of bar files")
  return parser

fep_PI_contractions_script=TinkerScript(
  'fep-ctr',fep_PI_contraction
  ,compose(populate_parser_bar,populate_parser_useb,populate_parser_contract)
  ,description="Compute Free energy difference between full and contracted polymer\
    based on a contracted trajectory"
  ,help="Compute Free energy difference for PI contractions"
)

bar_PI_contraction_script=TinkerScript(
  'bar-ctr',bar_PI_contraction_indirect
  ,compose(populate_parser_bar,populate_parser_useb,populate_parser_contract)
  ,description="Compute Free energy difference between full and contracted polymer\
    based on a contracted trajectory"
  ,help="Compute Free energy difference for PI contractions"
)





