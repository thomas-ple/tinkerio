#!/usr/bin/env python3
import argparse
import numpy as np
from pathlib import Path
from .properties import plot_properties,  \
                        plot_histogram, \
                        tinkerio_properties, \
                        tinkerio_property, \
                        print_available_properties, \
                        properties_header, \
                        PropertyNotFound
from .stats import blocking_std
from .util import TinkerScript

def analyze_tinkerfile(props,tinkerfile,**kwargs):
  if not isinstance(tinkerfile,Path): tinkerfile=Path(tinkerfile)
  kwargs["verbose"] = not kwargs["quiet"]
  kwargs["get_simulation_time"] = not kwargs["steps"]

  if not props:
    print_available_properties(tinkerfile)
    return

  if ("plot" in kwargs) and kwargs["plot"]:
    plot_properties(props,tinkerfile,**kwargs)
  elif ("histogram" in kwargs) and kwargs["histogram"]:
    plot_histogram(props,tinkerfile,**kwargs)
  else:
    data = tinkerio_properties(props,tinkerfile)
    simtime=False
    time = np.arange(data.shape[0]) 
    if kwargs["get_simulation_time"]:
      try:
        time = tinkerio_property("simulation time",tinkerfile)
        print("simulation time",time[-1],"ps",end="")
        simtime=True
      except PropertyNotFound:
        pass
    sim_interval=time[1]-time[0]
    thermalize = -1
    if "thermalize" in kwargs and kwargs["thermalize"] >0:
      thermalize = kwargs["thermalize"]
    selth=time>thermalize
    avg=np.mean(data[selth,:],axis=0)
    nsample=np.count_nonzero(selth)
    if "outfile" in kwargs and kwargs["outfile"] is not None:
      outfile = kwargs["outfile"]
      header = properties_header(props,simtime)
      footer = "avg= "+" ".join([str(a) for a in avg])
      np.savetxt(outfile,np.column_stack((time,data)),header=header,footer=footer)
    if simtime:
      ( print(" (averaging time",max(sim_interval,time[-1]-thermalize)," ps)") 
               if thermalize>0 else print(""))
    else:
      print("averaged on ",max(1,int(time[-1]-max(thermalize,0)))," frames")
    for i,prop in enumerate(props):
      if nsample>2:
        std_naive = np.std(data[selth,i])/np.sqrt(nsample-1)
        std=blocking_std(data[selth,i])
        eff_sampling = (std_naive/std)**2
        print(f'{prop:>20} {avg[i]:12.4f} +/- {2*std:<12.4f}'
             ,f' | sampling efficiency {100*eff_sampling:5.1f}%'
             ,f' ({int(nsample*eff_sampling)}/{nsample})' )
      else:
        print(prop,avg[i])

def populate_parser(parser:argparse.ArgumentParser):
  parser.add_argument("tinkerfile",help="Tinker output file",type=Path)
  parser.add_argument("props",help="Properties to extract",type=str,nargs="*")

  #group = parser.add_mutually_exclusive_group()
  parser.add_argument("-P","--plot",action='store_true',help="if present, plot time series of properties")
  parser.add_argument("-H","--histogram",action='store_true',help="if present, plot histograms")
  
  parser.add_argument("-i","--interval",type=float,help="refresh interval when watching")
  parser.add_argument("-t","--thermalize",type=float,default=0.,help="time (ps) or number of frames before starting to average")
  parser.add_argument("-l","--latest",type=float,help="plot only the latest l ps of simulation.")
  parser.add_argument("-q","--quiet",action='store_true',help="quiet output")
  parser.add_argument("-s","--steps",action='store_true',help="do not get simulation time")
  parser.add_argument("-w","--watch",action='store_true',help="watch output file")
  parser.add_argument("-o","--outfile",type=str,help="name of output file")

  parser.add_argument("--nbins",type=int,help="number of bins for histograms")
  parser.add_argument("--bw",type=float,help="smoothing bandwidth for KDE")
  return parser

def main():
  parser=argparse.ArgumentParser()
  populate_parser(parser)
  kwargs=vars(parser.parse_args())
  analyze_tinkerfile(**kwargs)

analyze_script=TinkerScript('analyze',analyze_tinkerfile,populate_parser,aliases=["a"]
                ,description="exctract average or watch tinker properties"
                ,help="analyze tinker output file")

if __name__ == "__main__":
  main()
