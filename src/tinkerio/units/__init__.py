from .tinker_units import *
  

def get_multiplier(unit_string,mapping):
  unit_string=unit_string.lower().strip()

  multiplier=1.

  unit_start=0
  unit_stop=0
  in_power=False
  power_start=0
  power_stop=0
  tmp_power=1.
  for i in range(len(unit_string)):

    current_char=unit_string[i]
    #print(current_char)
    if current_char=="^":

      if in_power:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError
        
      in_power=True
      unit_stop=i-1
      if unit_stop-unit_start < 0:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError

    elif current_char=="{":

      if not in_power:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError
      
      if i+1 >= len(unit_string):
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError

      power_start=i+1

    elif current_char=="}":

      if not in_power:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError
      
      in_power=False
      power_stop=i-1

      if power_stop-power_start < 0:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError
      else:
        tmp_power_read=int(unit_string[power_start:power_stop+1])
        tmp_power=tmp_power*tmp_power_read

      unit_substring=unit_string[unit_start:unit_stop+1]
      tmp_unit=mapping[unit_substring]

      multiplier=multiplier*(tmp_unit**tmp_power)

      power_start=0
      power_stop=0
      unit_start=i+1
      unit_stop=0
    
    elif current_char=="*":
      if in_power:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError

      if unit_start==i:
        unit_start=i+1
        tmp_power=1.
        continue

      unit_stop=i-1
      if unit_stop-unit_start < 0:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError

      unit_substring=unit_string[unit_start:unit_stop+1]
      tmp_unit=mapping[unit_substring]
      multiplier=multiplier*(tmp_unit**tmp_power)

      unit_start=i+1
      unit_stop=0
      tmp_power=1.
    
    elif current_char=='/':
      if in_power:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError

      if unit_start==i:
        unit_start=i+1
        tmp_power=-1.
        continue

      unit_stop=i-1
      if unit_stop-unit_start < 0:
        print("Error: Syntax error in unit '"+unit_string+"' !")
        raise ValueError

      unit_substring=unit_string[unit_start:unit_stop+1]
      tmp_unit=mapping[unit_substring]
      multiplier=multiplier*(tmp_unit**tmp_power)

      unit_start=i+1
      unit_stop=0
      tmp_power=-1.

    else:
      if i+1 >= len(unit_string):
        if in_power:
          print("Error: Syntax error in unit '"+unit_string+"' !")
          raise ValueError

        unit_stop=i
        if unit_stop-unit_start < 0 :
          print("Error: Syntax error in unit '"+unit_string+"' !")
          raise ValueError

        unit_substring=unit_string[unit_start:unit_stop+1]
        tmp_unit=units_map[unit_substring]
        multiplier=multiplier*(tmp_unit**tmp_power)
    
  return multiplier
