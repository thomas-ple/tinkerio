from numpy import pi

EV         = 27.211             # Hartree to eV
KCALPERMOL = 627.509            # Hartree to kcal/mol
BOHR       = 0.52917721         # Bohr to Angstrom
MPROT      = 1836.15            # proton mass
HBAR       = 1.                 # Planck's constant
FS         = 2.4188843e-2       # AU time to femtoseconds
PS         = FS/1000            # AU time to picoseconds
KELVIN     = 3.15774e5          # Hartree to Kelvin
THZ        = 1000./FS           # AU frequency to THz
NNEWTON    = 82.387             # AU Force to nNewton 
CM1        = 219471.52          # Hartree to cm-1 
GMOL_AFS   = BOHR/(MPROT*FS)    # AU momentum to (g/mol).A/fs
KBAR       = 294210.2648438959  # Hartree/bohr**3 to kbar
ATM        = KBAR*1000./1.01325 # Hartree/bohr**3 to atm
GPA        = 0.1*KBAR           # Hartree/bohr**3 to GPa

au_dict={
  "1": 1.,
  "au": 1.,
  "ev": EV,
  "kcalpermol": KCALPERMOL,
  "angstrom": BOHR,
  "bohr": 1./BOHR,
  "amu": 1./MPROT,
  "femtoseconds": FS,
  "fs": FS,
  "picoseconds": FS/1000.,
  "ps":FS/1000.,
  "kelvin": KELVIN,
  "k": KELVIN,
  "thz": THZ,
  "tradhz": THZ/(2*pi),
  "cm-1":CM1,
  "kbar":KBAR,
  "gmolafs":GMOL_AFS,
  "atm":ATM,
  "gpa":GPA,
}