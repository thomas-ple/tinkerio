from numpy import pi

AVOGADRO    = 6.02214129e+23     # Avogadro's number (N) in particles/mole
LIGHTSPD    = 2.99792458e-2      # speed of light in vacuum (c) in cm/ps
BOLTZMANN   = 0.831446215        # Boltzmann constant (kB) in g*Ang**2/ps**2/mole/K
GASCONST    = 1.98720415e-3      # ideal gas constant (R) in kcal/mole/K
EMASS       = 5.485799095e-4     # mass of an electron in atomic mass units
PLANCK      = 6.62606957e-34     # Planck's constant (h) in J-s
JOULE       = 4.1840             # conversion from calories to joules
CONVERT     = 4.1840e+2          # conversion from kcal to g*Ang**2/ps**2
BOHR        = 0.52917721092      # conversion from Bohrs to Angstroms
HARTREE     = 627.5094743        # conversion from Hartree to kcal/mole
EVOLT       = 27.21138503        # conversion from Hartree to electron-volts
EFREQ       = 2.194746313708e+5  # conversion from Hartree to cm-1
COULOMB     = 332.063714         # conversion from electron**2/Ang to kcal/mole
DEBYE       = 4.80321            # conversion from electron-Ang to Debyes
PRESCON     = 6.85684112e+4      # conversion from kcal/mole/Ang**3 to Atm
HBAR_PLANCK = (PLANCK*1.0e11*AVOGADRO)/(2.*pi)  # Planck's constant in g*Ang**2/ps**2/mole*ps
CM1         = 1./LIGHTSPD/(2.*pi)  # conversion from ps-1 to cm-1
