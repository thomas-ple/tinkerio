import argparse
from collections.abc import Iterable

from .analyze_tinkerfile import analyze_script
from .radial import radial_script
from .free_energies import bar_script,fep_script,alchemical_script
from .free_energies.fep_contractions import fep_PI_contractions_script,bar_PI_contraction_script

tinkerio_scripts=[
  analyze_script,
  radial_script,
  bar_script,
  fep_script,
  alchemical_script,
  fep_PI_contractions_script,
  bar_PI_contraction_script,
]

def main():
  parser = argparse.ArgumentParser(prog='TinkerIO')
  subparsers = parser.add_subparsers(description='Available scripts',required=True)

  script_parsers=[]
  #analyze
  for script in tinkerio_scripts:
    script_parser=subparsers.add_parser(script.name
      ,aliases=script.aliases
      ,description=script.description
      ,help=script.help)
    script.populate_parser(script_parser)
    script_parser.set_defaults(func=script.execute)
    script_parsers.append(script_parser)

  args=parser.parse_args()
  execute=args.func
  kwargs=vars(args)
  del kwargs["func"]
  execute(**kwargs)
