from io import TextIOWrapper
import sys
import re
from pathlib import Path
from time import sleep as time_sleep 
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.stats import gaussian_kde

from .util import read_file_or_string as rfos
from .util import cleanup_ax
from .stats import blocking_std, cumavg


__RE_FLOAT='-?[0-9]+\.[0-9]+(?:E[+-][0-9]+)?'
__RE_WORDS='[a-zA-Z_]+(?: *[a-zA-Z_])*'
__DEFAULT_INTERVAL=2.
__AVG_COLOR = "C1"
__DATA_COLOR = "C0"

class PropertyNotFound(Exception):
  pass

def get_available_properties(tinker_data):
  """ get the set of available properties in the tinker output file 
      or inside a string """
  properties=set(
    re.findall("^ *("+__RE_WORDS+")[ =:]*"+__RE_FLOAT
    ,rfos(tinker_data)
    ,flags=re.I|re.M)
  )
  return properties

def print_available_properties(tinker_data):
  properties = get_available_properties(tinker_data)
  print(f"List of available properties :")
  for prop in properties:
    print(f"- {prop}")

def tinkerio_property(prop,tinker_data):
  data=np.array(
    re.findall("^ *"+prop+"[ =:]*("+__RE_FLOAT+")"
    ,rfos(tinker_data)
    ,flags=re.I|re.M)
    ,dtype="float"
  )
  if len(data)>0: return data
  raise PropertyNotFound(f"Property '{prop}' was not found.")

def tinkerio_properties(props,tinker_data):
  data=[]
  for prop in props:
    data.append(tinkerio_property(prop,tinker_data))
  return np.array(data).T

def generate_properties(props,tinkerfile,sleep=time_sleep,get_simulation_time=False,watch=False,**kwargs):
  if not isinstance(tinkerfile,Path): tinkerfile=Path(tinkerfile)
  pf=open(tinkerfile,"r")
  #pf.seek(0,os.SEEK_END)
  outstr=""
  notfound=0
  propsfull=[prop.lower() for prop in props]
  interval = kwargs["interval"] if kwargs["interval"] else __DEFAULT_INTERVAL
  if get_simulation_time:
    if not watch:
      try:
        _ = tinkerio_property("simulation time",tinkerfile)
      except PropertyNotFound:
        get_simulation_time=False

    if "simulation time" not in propsfull:
      propsfull+=["simulation time"]
      indextime=len(propsfull)-1
    else:
      indextime=propsfull.index("simulation time")
  data=np.empty((0,len(propsfull)))
  while True:
    #newline=pf.readline()
    outstr=pf.read()
    if not outstr:
      sleep(interval)
      continue
    try:
      newdat = tinkerio_properties(propsfull,outstr)
      data = np.concatenate((data,newdat))
      notfound=0
      if get_simulation_time:
        yield data[:,:-1],data[:,indextime]
      else:
        yield data, np.arange(data.shape[0])
    except PropertyNotFound as err:
      if outstr:
        notfound+=1
        if not watch: 
          raise err
      if notfound>10:
        print(err)
    except GeneratorExit:
      print(f"closing {tinkerfile}")
      pf.close()
      return
    except KeyboardInterrupt:
      print(f"closing {tinkerfile}")
      pf.close()
      sys.exit(0)
    except Exception as err:
      print(err)

def setup_figures(props,exit_when_closed=False,**kwargs):
  #plt.ion()
  matplotlib.rcParams['figure.raise_window'] = False
  for i,prop in enumerate(props):
    fig=plt.figure(i)
    if exit_when_closed: 
      fig.canvas.mpl_connect('close_event',lambda e: sys.exit(0))
    plt.title(prop)
    plt.grid(True)

def properties_header(props,get_simulation_time=False):
  props_out = ["_".join(prop.split()) for prop in props] 
  header = " ".join(props_out)
  return "time "+header if get_simulation_time else "frame "+header
  

def plot_properties(props,tinkerfile,thermalize=0.,get_simulation_time=False,watch=False,histogram=False,outfile=None,verbose=False,**kwargs):
  nup=0
  if watch: print(f"Started streaming {props} from '{tinkerfile}'")
  setup_figures(props,watch)
  xlabel = "simulation time (ps)" if get_simulation_time else "frame number"
  writeout = outfile is not None
  header = properties_header(props,get_simulation_time)
  relshift= 1.02 # shift for plotting histogram on the right of data
  bw=kwargs["bw"] if "bw" in kwargs else None # bandwidth for the KDE
  thermalize=max(0,thermalize)
  for data,time in generate_properties(props,tinkerfile,plt.pause,get_simulation_time,watch=watch,**kwargs):
    nup+=1

    if thermalize>0:
      avg = np.concatenate((data[time<thermalize,:]
                      ,cumavg(data[time>=thermalize,:])))
    else:
      avg=cumavg(data)
    nsample=np.count_nonzero(time>=thermalize)

    if verbose:
      if watch: print("update ",nup,end=", ")
      print("Sample size ",nsample)
    if writeout: np.savetxt(outfile,np.column_stack((time,data))
                    ,header=header
                    ,footer = "avg= "+" ".join([str(a) for a in avg[-1,:]])
                  )
    if nsample>2:
      print(f'{"Property":>20} {"Average":>12}      2sig(95%)     1/Deff   Neff(Ns={nsample})')
    for i,prop in enumerate(props):
      plt.figure(i)
      #plt.clf()
      if nup >1: cleanup_ax(plt.gca())
      plt.plot(time,data[:,i],label="data",color=__DATA_COLOR)
      plt.plot(time,avg[:,i],label="cumulative avg",color=__AVG_COLOR)
      plt.xlabel(xlabel)

      histdat = data[time>=thermalize,i]
      if verbose and nsample<=2: print(prop,avg[-1,i])
      if nsample>2:
        std_naive = np.std(histdat)/np.sqrt(nsample-1)
        #plt.figure(len(props)+i)
        #plt.title("blocking "+prop)
        #ax=plt.gca()
        #if nup >1: cleanup_ax(ax)
        std=blocking_std(histdat)
        do_hist = std/abs(avg[-1,i])>1.e-5
        #plt.figure(i)
        eff_sampling = (std_naive/std)**2 if do_hist else 1.
        if verbose:
          avg_str=f'{avg[-1,i]:12.5f} +/- {2*std:<12.5f}'
          ratio_str=f'{int(nsample*eff_sampling)}/{nsample}'
          print(f'{prop:>20} {avg_str}   {100*eff_sampling:5.1f}%    {int(nsample*eff_sampling)}')
        timetherm=time[time>=thermalize]
        avgtherm=avg[time>=thermalize,i]
        nstd=min(100,nsample)
        stds=np.zeros(nstd)
        js = np.linspace(2,nsample,nstd,dtype="int")-1
        for k,j in enumerate(js):
          stds[k]=blocking_std(histdat[:j])
        plt.fill_between(timetherm[js],avgtherm[js]-2*stds,avgtherm[js]+2*stds,linewidth=0,color=__AVG_COLOR,alpha=.4,zorder=10)
        if histogram and do_hist:
          centers=np.linspace(avg[-1,i]-5*std,avg[-1,i]+5*std,100)
          dens_avg=np.exp(-(centers-avg[-1,i])**2/(2*std**2))
          x=time[-1]*(relshift+0.1*dens_avg)
          plt.fill_betweenx(centers,time[-1]*relshift,x,color=__AVG_COLOR,alpha=0.4,linewidth=0
                            ,label=f"avg posterior (blocking gaussian)",zorder=10)

      if histogram and nsample>2 and do_hist:
        #hist,edges=np.histogram(histdat,bins=100)
        #centers=0.5*(edges[:-1]+edges[1:])
        #plt.plot(time[-1]+hist,centers,label="histogram",color=__data_color)
        std=np.std(histdat)
        centers=np.linspace(avg[-1,i]-3*std,avg[-1,i]+3*std,100)
        #centers=np.linspace(np.min(histdat),np.max(histdat),100)
        kde=gaussian_kde(histdat,bw_method=bw)
        dens=kde(centers)
        avghist = np.average(centers,weights=dens)
        maxlikelihood = centers[np.argmax(dens)]
        avgerr=np.abs(avghist-maxlikelihood)
        x=time[-1]*(relshift+0.1*dens/np.max(dens))

        #plt.plot(x,centers,label=f"density (KDE bw={bw})",linewidth=3,alpha=0.6)
        plt.fill_betweenx(centers,time[-1]*relshift,x,linewidth=0,alpha=0.6,color=__DATA_COLOR
                          ,label=f"data density (KDE bw={bw})")
        #plt.axvline(time[-1]*relshift,color="k",alpha=0.2)
        #plt.fill_between(time,avghist-avgerr,avghist+avgerr,color=__avg_color,alpha=0.2)
        #plt.axhline(avghist,color=__avg_color,alpha=0.5,label=f"avg from density")
      
      #plt.legend() #bbox_to_anchor=(0, 1.05, 1, 0), loc="lower left", mode="expand", ncol=2)
      #plt.tight_layout()

    plt.show(block=not watch)

    if not watch:
      return data,time
    elif verbose:
      print()

def plot_histogram(props,tinkerfile,thermalize=0.,get_simulation_time=False,watch=False,verbose=False,**kwargs):
  nup=0
  if watch: print(f"Started streaming histograms of {props} from '{tinkerfile}'")
  setup_figures(props,watch)
  for data,time in generate_properties(props,tinkerfile,plt.pause,get_simulation_time,watch=watch,**kwargs):
    nup+=1
    if verbose and watch: print("update ",nup)
    if(thermalize>0): data = data[time>=thermalize]

    if data.shape[0]<2:
      if watch: continue
      print("not enough data to make histogram")
      return data,None
    for i,prop in enumerate(props):
      plt.figure(i)
      #plt.clf()
      if nup >1:
        cleanup_ax(plt.gca())
      avg = np.mean(data[:,i])  
      std=blocking_std(data[:,i])
      if verbose: print(prop,avg,'+/-',2*std)
      #std=np.std(data[:,i]-avg) 
      #hist,edges=np.histogram(data[:,i],bins=100,density=True)
      #centers=0.5*(edges[:-1]+edges[1:])
      #plt.plot(centers,hist,label="histogram")
      centers=np.linspace(np.min(data[:,i]),np.max(data[:,i]),100)
      centers_avg=np.linspace(avg-5*std,avg+5*std,100)
      nbins=kwargs["nbins"] if kwargs["nbins"] else 30
      bw=kwargs["bw"] if kwargs["bw"] else None
      kde=gaussian_kde(data[:,i],bw_method=bw)
      dens=kde(centers)
      dens_avg=np.exp(-(centers_avg-avg)**2/(2*std**2))*max(dens)
      plt.hist(data[:,i],bins=nbins,density=True,label="histogram",color=__DATA_COLOR,alpha=0.6)
      plt.plot(centers,dens,label=f"data density (KDE bw={bw})",linewidth=3,color=__DATA_COLOR)
      plt.fill_between(centers_avg,np.zeros_like(dens_avg),dens_avg,label=f"avg posterior (blocking gaussian, scaled)",color=__AVG_COLOR,alpha=0.5)
      plt.axvline(avg,color=__AVG_COLOR)
      plt.legend(bbox_to_anchor=(0, 1.05, 1, 0), loc="lower left", mode="expand", ncol=2)
      plt.tight_layout()
    
    plt.show(block=not watch)
    if not watch:
      return data,kde
    elif verbose:
      print()






