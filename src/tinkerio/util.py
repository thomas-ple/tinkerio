import os
import io
from typing import Tuple
import numpy as np
from matplotlib.pyplot import Axes
from typing import Callable,List,Union
from dataclasses import dataclass
import functools

@dataclass
class TinkerScript: 
  name:str
  execute:Callable
  populate_parser:Callable = lambda parser: parser
  aliases:list = ()
  description:str = None
  help:str = None

def read_file_or_string(input) -> str:
  """ Return a string from an input.
    If the input is a :
      - string => return the input directly
      - path => open and read the file and return the content
      - file handle => read the file and return the content
  """

  # string input => just return the input
  if isinstance(input,str): return input

  # path object => open and return content
  if isinstance(input,os.PathLike):
    with open(os.fspath(input),"r") as f: return f.read()
  
  # File handle => return content
  if isinstance(input, io.TextIOBase): return input.read()
    
  raise ValueError("Error: only PathLike, str or TextIOBase inputs are accepted")

def cleanup_ax(ax:Axes)->None:
  ax.set_prop_cycle(None)
  for line in ax.get_children():
    try:
      line.remove()
    except:
      pass

def skip2range(t:Tuple[int,int,int],max_frame=None):
  """ convert a Fortran range in Tuple shape to numpy range 
    (Fortran range: indexing starts at 1 and range is inclusive both ways)
  """
  if t is None: return None,None,None
  b,e,s=t
  if max_frame is not None: e = min(max_frame,e)
  if e==0: e=None
  assert b>=0, "the number of skipped frames should be >=0"
  return b,e,s

def compose(*funcs):
  """ Compose an arbitrary number of functions.
  
  For example: compose(f,g,h) = lambda x: f(g(h(x)))
  """
  def f_after_g(f,g): return lambda x: f(g(x))
  return functools.reduce(f_after_g,funcs,lambda x:x)





